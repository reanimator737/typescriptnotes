//Тут будет чуток примеров для редакса

type Nullable<T> = null | T;

type UserType = {
    age: number,
    name: string,
    lastName: string,
}

type PhotoType = {
    large: string,
    small: string
}

//Юзаем as только для формирования типов автоматически
const initialState = {
    user: null as Nullable<UserType>,
    photo: null as Nullable<PhotoType>,
}

//Не нужно писать интерфейс для стейта, ts генерит его самостоятельно
type StateType = typeof initialState;



//Типизация action creators


// ReturnType = {type: string, age: number}
const AC1 = (age: number) => ({type: 'SET-AGE', age});

const AC1Strict = (age: number) => ({type: 'SET-AGE', age} as const) ;

// ReturnType = {firstName: string, lastName: string, type: "Set-Name-And-LASTNAME"}
const AC2 = (firstName: string, lastName: string) => ({type: 'Set-Name-And-LASTNAME', firstName, lastName} as const)

type AC1Type =  ReturnType<typeof AC1>
type AC2Type =  ReturnType<typeof AC2>

//В тип может попасть что угодно
const action1: AC1Type = {type: "sadasd", age: 24}

//А тут в тип уже не поменяешь
const action2: AC2Type = {type: 'Set-Name-And-LASTNAME', firstName: 'rwerewr', lastName: 'fsdfsdfsdf'}


type ActionsTypes = ReturnType<typeof AC1Strict> | AC2Type

const reducer = (state: StateType = initialState, action: ActionsTypes) => {
    switch (action.type) {
        case "SET-AGE":
            return {...state, age: action.age}
        case "Set-Name-And-LASTNAME" :
            return {...state, name: action.firstName + ' ' + action.lastName}
    }
}






//А тут ну чисто черная магия
//Я сам не уверен как это работает, но мы принимаем в функцию call (functionToCall, ...args)
//Из них вытягиваем ReturnType (возвращает functionToCall) и ArgumentsType (из ...args,  а там max: number)
//<ArgumentsType extends any[] (это массив с 1 значение max: number и он подходит под массив эни), ReturnType> это и есть эта шляпа
// и потом возвращаем только ReturnType который нашли раньше
// PS extends в generic проверяет подходить ли тип (string[] extends any[] but string[] dont extends number[])
function call<ArgumentsType extends any[], ReturnType>(
    functionToCall: (...args: ArgumentsType) => ReturnType, ...args: ArgumentsType
): ReturnType {
    return functionToCall(...args);
}

function getRandomInteger(max: number) {
    return Math.floor(Math.random() * max);
}

const randomNumber = call(getRandomInteger, '100');
const randomNumber2 = call(getRandomInteger, 100);

