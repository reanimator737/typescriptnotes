// https://www.typescriptlang.org/docs/handbook/2/conditional-types.html

type UserType = {
    age: number,
    name: string,
    lastName: string,
}

type PhotoType = {
    large: string,
    small: string
}

// T === 'user' ? UserType: PhotoType
type Smth<T> = T extends 'user' ? UserType : PhotoType;

let a: Smth<'user'> = {
    age: 12,
    lastName: 'asdasd',
    name: 'das342'
}

let b: Smth<'photo'> = {
    large: 'dasdasd',
    small: 'asdasdasd'
}

// Пробегает в тернарном сначала user, потом photo. Находит для 2 типы по этому тут 2 типа
let c : Smth<'user' | 'photo'> = {
    small: 'dsfsdf',
    name: 'adsasdasd',
    large: 'dasdasd',
    lastName: 'asdasdasd',
    age: 5,
}


const obj = {
    a: {name: 'Sasha'},
    b: {age: 25},
    c: {site: {title: 'site.com'}}
}

// Работают SomeTypes и SomeTypes2 одинаково, но SomeTypes2 сам проходит по всем ключам и сам определяет тип
type SomeTypes = typeof obj.a | typeof obj.b | typeof obj.c


// Немного про infer https://www.youtube.com/watch?v=SpQDK74vLKo
// infer - определяет переменную в пределах нашего ограничения типа на которую можно ссылаться или возвращать.
// Использоваться может только в условных типах после слова extend

//Что тут написано.
// Прими тип T, если он вида объект ключ которого строка, а значение тс определит сам, то верни значение, которое вернул тс или never
type SomeTypes2<T> =  T extends {[key: string] : infer U } ? U : never


//Ну и пример для редакса, можно посмотреть как было в 1 файлике

const actions = {
    AC1: (age: number) => ({type: 'SET-AGE', age} as const),
    AC2: (firstName: string, lastName: string) => ({type: 'Set-Name-And-LASTNAME', firstName, lastName} as const)
}

type Nullable<T> = null | T;

const initialState = {
    user: null as Nullable<UserType>,
    photo: null as Nullable<PhotoType>,
}

type StateType = typeof initialState;

type ActionsTypes =  ReturnType<SomeTypes2<typeof actions>>

const reducer = (state: StateType = initialState, action: ActionsTypes) => {
    switch (action.type) {
        case "SET-AGE":
            return {...state, age: action.age}
        case "Set-Name-And-LASTNAME" :
            return {...state, name: action.firstName + ' ' + action.lastName}
    }
}

