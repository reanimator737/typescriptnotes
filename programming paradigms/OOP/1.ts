//Некий список характеристик => класс
class Rectangle {
    //Свойства класса
    width;
    heigth;

    //Спецальный метод, который вызывается при создании объектов.
    //Сюда передаем те значения свойств, которые хотим добавить экземпляру класса
    constructor(w, h) {
        this.width = w;
        this.heigth = h;
    }


    //Методы класса
    calcArea() {
        // this ссылается на контекст объекта который будет экземпляром этого класса.
        // То бишь react1.calcArea() => return react1.width * react1.heigth
        return this.width * this.heigth;
    }
}

// Конкретный представитель класса (объект), его экземпляр
// Кстати, new всегда возращает объект
const rect1 = new Rectangle(5,10);

rect1.calcArea(); //50




/*
    ООП строится на 3 основных парадигмах:
    1) инкапсуляция - объединение данных и кода, манипулирующий зтими данными
    2) наследование - один объект может приобретать свойства другого. Это позволяет нам строить иерархию классов
    3) полиморфизм - возможность дополнять объект функционалом. Классический полиморфизм — замещение,
    переопределение методов. Ad hoc полиформизм — перегрузка методов, поведение в зависимости от данных.

    Так же часто выделяют 4 парадигму.
    4) абстракция - это придание объекту характеристик, которые четко выделяет его на фоне остальных,
     определяя его концептуальные границы.
 */





// Рядом с инкапсуляцией всегда идет сокрытие - разграничении доступа различных частей программы
// ко внутренним компонентам друг друга
class RectanglePrivate {
    private _width;
    private _heigth;

    /*
    Существует 3 модификатора доступа которые можно добавить и к методу и к свойству:
    1)publick - default, поля и методы доступны из вне
    2)protected - определяет поля и методы, которые из вне класса видны только в классах-наследниках
    3)private - нельзя будет обратиться извне при создании объекта данного класса
    */

    constructor(w, h) {
        this._width = w;
        this._heigth = h;
    }

    calcArea() {
        return this._width * this._heigth;
    }

    //Для получения доступа к приватным свойствам из вне создают специальные методы - геттеры и сеттеры.
    // Мы сами контролируем нужду читать и менять свойства
    get width() {
        return this._width;
    }

    set width(value) {
        this._width = value;
    }
    // Получить высоту можем, а поменять из вне нет.
    get heigth() {
        return this._heigth
    }
}

const rectP1 = new RectanglePrivate(5,10);
//Property '_width' is private and only accessible within class 'RectanglePrivate'.

//Доступно тк есть геттер. Не нужно вызывать как обычный метод, приятная фича синтаксиса.
rectP1.width
rectP1.calcArea(); //50




// Наследование (очень помогает переиспользовать код)

class Person {
    private _firstName;
    private _lastName;
    private _age;
    constructor(firstName, lastName, age) {
        this._firstName = firstName;
        this._lastName = lastName;
        this._age = age;
    }

    //Метод для полиморфизма
    public greeting() {
        console.log(`Привет я человек и меня зовут ${this._firstName}`)
    }

    get firstName() {
        return this._firstName;
    }
    set firstName(value) {
        this._firstName = value;
    }

    get lastName() {
        return this._lastName;
    }
    set lastName(value) {
        this._lastName = value;
    }

    get age() {
        return this._age;
    }
    set age(value) {
        if (value >= 0 ) {
            this._age = value;
        } else {
            this._age = 0;
        }
    }
}


//Расширяем класс Person. Принимает все методы и свойства класса от которого наследуется.
class Employee extends Person {
    private _inn;
    private _number;

    constructor(firstName, lastName, age, inn, number) {
        //Super вызывает родительский конструктор
        super(firstName, lastName, age);
        this._inn = inn;
        this._number = number;
    }
    public get fullName() {
        return `Фамилия - ${this.lastName}. Имя - ${this.firstName}`
    }

    //Переопределяем метод который был у родителя (це полиморфизм)
    public greeting() {
        console.log(`Привет я работник и меня зовут ${this.firstName}`)
    }
}

const empl1 = new Employee('dsf', 'sdf', 15, 5234234, '4234234234');

empl1.fullName

//Полиморфизм (один и тот же код работает с разными типами данных)
