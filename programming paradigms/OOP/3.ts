//Интерфейсы и абстрактные классы
// Есть описание но нет реализации - интерфейс
// Абстрактный класс тоже самое, но можно впихнуть часть реализации( нужны для наследованиея)

interface Reader {
    read(url);
}

interface Writer{
    write(data);
}

class FileClient implements Reader, Writer {
    read(url) {

    }

    write(data) {
    }
}



abstract class User {
    connect (url: string) : string {
        return 'user connected'
    }

    abstract getData(): object;
}

class Admin extends User {
    getData(): object {
        return {adminInfo: []};
    }
}

class Client extends User {
    getData(): object {
        return {clientInfo: []};
    }
}

const admin = new Admin();

admin.connect('token');
