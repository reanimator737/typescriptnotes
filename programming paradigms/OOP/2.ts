//Композиция и агрегация

class Engine {
    drive() {
        console.log('Двигатель работает')
    }

}

class Wheel {
    drive() {
        console.log('Колесо едет')
    }
}

class  Freshener  {
}


class Car {
    engine: Engine;
    wheels: Wheel[];
    freshener: Freshener;

    constructor(freshener) {
        //Агрегация, свойство береться из вне. Один класс включает в себя другой класс в качестве одного из полей
        this.freshener = freshener
        //Композиция - создается при создании автомобиля и полностью управляется автомобилем
        this.engine = new Engine();
        this.wheels.push(new Wheel());
        this.wheels.push(new Wheel());
        this.wheels.push(new Wheel());
        this.wheels.push(new Wheel());
    }

    //делегирование
    drive() {
        this.engine.drive();
        for (let i = 0; i < this.wheels.length; i++) {
            this.wheels[i].drive()
        }
    }
}
