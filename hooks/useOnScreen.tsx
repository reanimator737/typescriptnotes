export default function useOnScreen(ref) {

    const [isIntersecting, setIntersecting] = useState(false)

    const observer = new IntersectionObserver(
        ([entry]) => setIntersecting(entry.isIntersecting)
    )

    useEffect(() => {
        observer.observe(ref.current)
        return () => { observer.disconnect() }
    }, [ref])

    return isIntersecting
}


//Usage
const DummyComponent = () => {

    const ref = useRef()
    const isVisible = useOnScreen(ref)

    return <div ref={ref}>{isVisible && `Yep, I'm on screen`}</div>
}
