// https://www.codewars.com/kata/593ff8b39e1cc4bae9000070/train/javascript

// Тут еще оптимизировать и оптимизировать, но я устал

function lcs(a, b) {
    if (a === '' || b === '') {
        return ''
    }
    let lengthA = a.length;
    let lengthB = b.length;

    if (lengthA < lengthB) {
        [a, b] = [b, a];
        [lengthA, lengthB] = [lengthB,lengthA]
    }

    a = a.split('');
    b = b.split('');
    const matrix = Array(lengthA + 1).fill().map(() => Array(lengthB + 1).fill(0));
    for (let i = 1; i < lengthA + 1; i++) {
        for (let j = 1; j < lengthB + 1; j++) {
            if (a[i - 1] === b[j - 1]) {
                matrix[i][j] = 1 + matrix[i - 1][j - 1];
            } else {
                matrix[i][j] = Math.max(matrix[i - 1][j], matrix[i][j - 1]);
            }
        }
    }

    const LCS = [];
    let rowIndex = lengthA--;
    let columnIndex= lengthB--;

    while (true){
        debugger
        if (matrix[rowIndex][columnIndex] === 0) {
            break;
        }

        if (matrix[rowIndex][columnIndex] === matrix[rowIndex - 1][columnIndex]) {
            rowIndex--;
        } else if (matrix[rowIndex][columnIndex] === matrix[rowIndex][columnIndex - 1]) {
            columnIndex--;
        } else {
            LCS.push(b[columnIndex - 1]);
            columnIndex--;
            rowIndex--;
        }

    }
    return LCS.reverse().join('')

}

console.log(lcs('notatest', 'anothertest'));
