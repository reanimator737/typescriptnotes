function differentiate(f) {
    const a = f.split('^');
    const b = [a[0].split('x'), a[1]];
    if (b[0][0] === '-') {
        b[0][0] = '-1'
    } else if (b[0][0] === '') {
        b[0][0] = '1'
    }

    if (b[0].length !== 2) {
        return 0
    } else if (b[1] === undefined) {
        return b[0][0]
    } else {
        let answer = +b[1] !== 2 ? `${+b[0][0] * +b[1]}x^${+b[1] - 1}` : `${+b[0][0] * +b[1]}x`;
        let array = answer.split('x');
        if (array[0] === '1') {
            array[0] = ''
            answer = array.join('x');
        } else if (array[0] === '-1') {
            array[0] = '-'
            answer = array.join('x')
        }
        return answer
    }
}

console.log(differentiate('x^-1'))
