//https://www.codewars.com/kata/536a155256eb459b8700077e
function createSpiral(N) {
    if ( typeof N !== "number" || !Number.isInteger(N) || N < 1 ){
        return []
    }
    let matrix = [];
    for (let i = 0; i < N; i++) {
        matrix[i] = new Array(N).fill('');
    }
    let x = 0;
    let y = 0;
    let number = 1;
    matrix[x][y] = number;
    let status = 0

    while (true) {
        status = 1;
        while (matrix[x][y + 1] === '') {
            number++;
            y++;
            matrix[x][y] = number;
            status = 0;
        }
        while (matrix[x + 1] && matrix[x + 1][y] === '') {
            number++;
            x++;
            matrix[x][y] = number;
            status = 0;
        }
        while (matrix[x][y - 1] === '') {
            number++;
            y--;
            matrix[x][y] = number;
            status = 0;
        }
        while (matrix[x - 1] && matrix[x - 1][y] === '') {
            number++;
            x--;
            matrix[x][y] = number;
            status = 0;
        }

        if (status === 1) {
            break;
        }
    }
    return matrix
}



console.log(createSpiral(10))

